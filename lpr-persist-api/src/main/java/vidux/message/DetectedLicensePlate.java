package vidux.message;

import java.util.UUID;

public class DetectedLicensePlate {

	final String plate;
	final long detectedAt;
	final UUID uuid = UUID.randomUUID();
	
	public DetectedLicensePlate(String plate, long detectedAt) {
		this.plate = plate;
		this.detectedAt = detectedAt;
	}
	
	public String getPlate() {
		return plate;
	}
	
	public long getDetectedAt() {
		return detectedAt;
	}
	
	@Override
	public String toString() {
		return detectedAt + " - " + plate;
	}
}
