package vidux;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.NotUsed;
import vidux.message.DetectedLicensePlate;

public interface LprPersistService extends Service {
	
	ServiceCall<DetectedLicensePlate, NotUsed> saveLpr();
	
	default Descriptor descriptor() {
		return Service.named("lpr-persist")
				.withCalls(Service.namedCall("lpr", this::saveLpr));
	}
}
