package vidux.detect;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.pubsub.PubSubRef;
import com.lightbend.lagom.javadsl.pubsub.PubSubRegistry;
import com.lightbend.lagom.javadsl.pubsub.TopicId;

import akka.actor.ActorSystem;
import scala.concurrent.duration.Duration;
import vidux.LprPersistService;
import vidux.message.DetectedLicensePlate;

public class LprDetector {

	ActorSystem actorSystem;
	PubSubRegistry pubSubRegistry;
	LprPersistService lprPersistService;
	
	@Inject
	public LprDetector(ActorSystem actorSystem, PubSubRegistry pubSubRegistry, LprPersistService lprPersistService) {
		this.actorSystem = actorSystem;
		this.pubSubRegistry = pubSubRegistry;
		this.lprPersistService = lprPersistService;
	}
	
	public void startDetect() {
		actorSystem.scheduler().schedule(
				Duration.create(0, TimeUnit.SECONDS),
				Duration.create(1, TimeUnit.SECONDS),
				new Runnable() {
					public void run() {
						String plate = LprUtils.randomLicense();
						long detectedAt = ZonedDateTime.now().toEpochSecond();
						DetectedLicensePlate detectedLicensePlate = new DetectedLicensePlate(plate, detectedAt);
						
						// publish
						PubSubRef<DetectedLicensePlate> topic = 
								pubSubRegistry.refFor(TopicId.of(DetectedLicensePlate.class, null));
						topic.publish(detectedLicensePlate);
						
						// save with service
						lprPersistService.saveLpr().invoke(detectedLicensePlate);
					}
				},
				actorSystem.dispatcher());
	}
	
}
