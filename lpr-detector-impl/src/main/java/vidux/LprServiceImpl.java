package vidux;

import java.util.concurrent.CompletableFuture;

import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.pubsub.PubSubRef;
import com.lightbend.lagom.javadsl.pubsub.PubSubRegistry;
import com.lightbend.lagom.javadsl.pubsub.TopicId;

import akka.NotUsed;
import akka.stream.Materializer;
import akka.stream.javadsl.Source;
import vidux.detect.LprDetector;
import vidux.message.DetectedLicensePlate;

public class LprServiceImpl implements LprService {

	LprDetector detector;
	PubSubRegistry pubSubRegistry;
	
	@Inject
	public LprServiceImpl(LprDetector detector, PubSubRegistry pubSubRegistry, Materializer mat) {
		this.detector = detector;
		this.pubSubRegistry = pubSubRegistry;
		detector.startDetect();
	}
	
	@Override
	public ServiceCall<NotUsed, Source<DetectedLicensePlate, ?>> lprStream() {
		return req -> {
			PubSubRef<DetectedLicensePlate> topicRef = pubSubRegistry.refFor(TopicId.of(DetectedLicensePlate.class, null));
			return CompletableFuture.completedFuture(topicRef.subscriber());
		};
	}
	
}
