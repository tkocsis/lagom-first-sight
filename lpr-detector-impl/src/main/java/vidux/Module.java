package vidux;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import vidux.detect.LprDetector;

public class Module extends AbstractModule implements ServiceGuiceSupport {

	@Override
	protected void configure() {
		bind(LprDetector.class);
		bindServices(serviceBinding(LprService.class, LprServiceImpl.class));
		bindClient(LprPersistService.class);
	}
}
