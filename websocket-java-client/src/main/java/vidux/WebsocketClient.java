package vidux;

import java.util.concurrent.ExecutionException;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;

public class WebsocketClient {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient();
	    client.websocket(7000, "localhost", "/lpr/stream", websocket -> {
	        websocket.handler(data -> {
	          System.out.println(data.toString("ISO-8859-1"));
	        });
	        
	        websocket.exceptionHandler(ex -> {
	        	ex.printStackTrace();
	        	client.close();
	        });
	        
	        websocket.endHandler(event -> {
	        	System.out.println("End");
	        	client.close();
			});
	    });
	}
}
