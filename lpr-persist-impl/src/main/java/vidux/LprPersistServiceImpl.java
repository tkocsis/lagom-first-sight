package vidux;

import java.util.concurrent.CompletableFuture;

import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.NotUsed;
import vidux.message.DetectedLicensePlate;

public class LprPersistServiceImpl implements LprPersistService {
	
	public LprPersistServiceImpl() {
	}
	
	@Override
	public ServiceCall<DetectedLicensePlate, NotUsed> saveLpr() {
		return licensePlate-> {
			System.out.println("Saving LPR: " + licensePlate);
			return CompletableFuture.completedFuture(NotUsed.getInstance());
		};
	}

}
