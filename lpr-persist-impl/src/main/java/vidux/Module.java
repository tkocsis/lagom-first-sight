package vidux;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

public class Module extends AbstractModule implements ServiceGuiceSupport {

	@Override
	protected void configure() {
		bindServices(serviceBinding(LprPersistService.class, LprPersistServiceImpl.class));
	}
}
