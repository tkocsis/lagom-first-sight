package vidux;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.NotUsed;
import akka.stream.javadsl.Source;
import vidux.message.DetectedLicensePlate;

public interface LprService extends Service {

	ServiceCall<NotUsed, Source<DetectedLicensePlate, ?>> lprStream();
	
	default Descriptor descriptor() {
		return Service.named("lpr-service")
				.withCalls(Service.namedCall("/lpr/stream", this::lprStream))
				.withAutoAcl(true);
	}
}
